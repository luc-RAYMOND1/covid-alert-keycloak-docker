FROM jboss/keycloak:latest

COPY docker-entrypoint.sh /opt/jboss/tools

EXPOSE 80

ENTRYPOINT [ "/opt/jboss/tools/docker-entrypoint.sh" ]
USER root
CMD ["-b", "0.0.0.0"]
