# covid-alert-keycloak-docker

Keycloak project used to launch keycloak on dokku with docker for the Covid Alert project (IWA)

## Installation

`> git clone https://gitlab.com/luc-RAYMOND1/covid-alert-kafka.git`

Based on this [project](https://github.com/cowofevil/keycloak-dokku.git), to deploy keycloak on Dokku.

## Production

Keycloak admin interface deployed, and available [here](https://covid-alert-keycloak-ig.cluster-ig5.igpolytech.fr/).